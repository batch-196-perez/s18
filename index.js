console.log("Hello World");

//  SUM

function printSum(num1,num2){
	let sum = num1 + num2;	
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum)
}

printSum(5,15);

// DIFFERENCE
function printDiff(num1,num2){
	let diff = num1 - num2;	
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(diff);
}

printDiff(20,5);

// PRODUCT
function printProduct(num1,num2){
	let productOfNum = num1 * num2;	
	console.log("The product of " + num1 + " and " + num2);
	return(productOfNum);
}

let product = printProduct(50,10);

console.log(product);

// QUOTIENT
function printQuotient(num1,num2){
	let quotient = num1 / num2;	

	console.log("The quotient of " + num1 + " and " + num2);
	return(quotient);
}

let quotient = printQuotient(50,10);
console.log(quotient);

//  AREA OF CIRCLE

function printCircleArea(providedRadius){
	let circleSize = 3.1416 *(providedRadius**2);

	console.log("The result of getting the area of a circle with " + providedRadius + "radius");
	return(circleSize);
}

let circleArea = printCircleArea(15);
console.log(circleArea)

// AVERAGE

function printAverage(num1,num2,num3,num4){
	let average = (num1 + num2 + num3 + num4 ) / 4;
	console.log("The average of " + num1 + " , " + num2  + " , " + num3 + " and " + num4);
	return(average);
}

let averageVar = printAverage(20,40,60,80);
console.log(averageVar);

// PERCENTAGE

function printPercentage(score,totalScore){
	let totalPercentage = (score/totalScore) *100;
	let isPassed = totalPercentage>=75;
	console.log("Is " + score + "/" + totalScore + "a passing score?")
	return(isPassed);
}

let isPassingScore = printPercentage(38,50);
console.log(isPassingScore);